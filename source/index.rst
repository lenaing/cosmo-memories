#######
Welcome
#######

.. image:: _static/FFVII.svg
   :align: center
   :width: 75%

Welcome to *Cosmo Memories* : A collection of reference sources about Final Fantasy VII.

This is an open source project, main repository is `here <https://gitlab.com/lenaing/cosmo-memories>`_.

.. toctree::
   :caption: Projects:
   :hidden:

   FF7 Reference/index.rst
   FF7 Technical Reference/index.rst

Copyrights / Licenses
---------------------

* Final Fantasy VII : ©1997 Square Enix Co., LTD. All Rights Reserved. Character design: ©1997 Tetsuya Nomura, Logo & Image Illustration: ©1997 Yoshitaka Amano.
* Final Fantasy VII SVG Logo : `eldi13 <https://www.deviantart.com/eldi13/art/Final-Fantasy-VII-logo-244710906>`_ CC BY-NC-ND 3.0
* Fam Fam Fam Silk Icons : `Mark James <http://www.famfamfam.com/lab/icons/silk/>`_ CC BY 3.0
