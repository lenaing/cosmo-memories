*************
FF7 Reference
*************

About this project
******************

This project is intended to be a gameplay reference about Final Fantasy VII.
It is meant to be paired with the technical reference to provide a complete
overview of the game to all tech-oriented or curious players.