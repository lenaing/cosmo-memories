Kernel
******

A bit of history
================

.. rubric:: Final Fantasy I, NES and Memory Mappers

The kernel is a throwback to the very first Final Fantasy game for the
Nintendo's original 8-bit system, the `Nintendo Entertainment System
<https://en.wikipedia.org/wiki/Nintendo_Entertainment_System>`_.

The NES could only natively read 32 Kib of program ROM.
To get around this incredible limitation, Nintendo developped "Memory Mappers"
that allowed parts of the program to be switched out, or "banked", and replaced
with other parts stored on the game cartridge.

Final Fantasy I used the Nintendo's *Memory Manager Controller #1* chip or
`MMC1 <https://en.wikipedia.org/wiki/Memory_management_controller#MMC1>`_.

The maximum size an MMC1 program's code could be was 256 Kib, so the controller
split the program ROM into 16 sections, each of 16 Kib long. The controller also
split the accessible memory from the cartridge into 2 16 Kib sections.

For FF1, the top 16 Kib was switchable but the bottom one could never be
switched out and stayed in memory until you shut down the system.
The original FF1 kernel was located into this 16 Kib portion of the memory.

.. rubric:: Kernel functionalities

First and foremost, the kernel contained the main program loop.

It handled all the low level functions for the game. Some of these included
controlling interrupts, banking in and out the appropriate parts of the game,
jumping control to a particular module, playing music and other tasks.

.. rubric:: Next game generations

As the Final Fantasy franchise grew, so did the size of the games.
They all still retained the kernel / module system.

During the porting process of old games to new generation consoles, this did
cause a few headaches.
For example, Final Fantasy VI was originally developped for the Super Nintendo.
When it's menu module was banked in, it was done with electronic bank switching.
The later PSX port banked the data from the CD-ROM, which caused an unexpected
lag that one was not used to.

On the PC port of FF7, the menu system was integrated into the main
executable.

FF7 Kernel functionalities
==========================

The kernel is a threaded multitasking program that manages the whole system.

It uses a simple software based memory manager that handles both RAM and video
memory for all the modules in the game.

Assisting the kernel are many statically or dynamically linked libraries.

.. rubric:: PSX

PSX used the Psy-Q SDK created by `Psygnosis <https://en.wikipedia.org/wiki/Psygnosis>`_,
so the kernel is statically linked to many Psy-Q libraries.

.. rubric:: PC port

The PC port replaced the PSX Psy-Q libraries with PC alternatives.
For example the SEQ player, used on the PSX to play music, was replaced with a
MIDI player. Both accomplish the same tasks, just with different file formats
and execution strategies.

.. include::
    Kernel/ram.rst

.. include::
    Kernel/vram.rst

.. include::
    Kernel/cdrom.rst

Game resouces
=============

The kernel reside is in on or multiple files, depending on the FF7 version.

.. toctree::
    Kernel/Resources/kernel_bin
    Kernel/Resources/kernel2_bin

.. rubric:: Sources

* `Qhimm's Wiki: Kernel Overview <http://wiki.ffrtt.ru/index.php?title=FF7/Kernel/Overview>`_
* `Qhimm's Wiki: Kernel Memory management <http://wiki.ffrtt.ru/index.php?title=FF7/Kernel/Memory_management>`_
