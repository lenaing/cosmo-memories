Attacks Visual Effects
**********************

.. list-table::
   :widths: 10 20 20
   :header-rows: 1

   * - ID
     - List Of Attacks
     - Description
   * - 04
     - Laser Cannon, Mono Laser
     - Laser rays appear from attacker weapon toward enemy
   * - 22
     - Beam Gun
     - A lot of pieces of ice fly towards enemy
   * - 64
     - Escape, Smoke Shot
     - A lot of smoke clouds appears
   * - 75
     - Search Scope
     - Attack used by Guard Scorpion
   * - 76
     - Tail Laser
     - Attack used by Guard Scorpion

.. rubric:: Sources

* `Qhimm's Wiki <http://wiki.ffrtt.ru/index.php?title=FF7/Battle/Attack_Effect_Id_List>`_