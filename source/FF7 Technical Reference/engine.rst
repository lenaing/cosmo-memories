Engine Basics
*************

A modular engine
================

The Final Fantasy VII engine is split and powered by several modules.

Not only it created very diverse gameplays, game environments and provided fluid
game transitions to the player, but it also enabled a great flexibility to the
Square programming team.

The team could break apart into very distinct groups to work on specific
subjects. It allowed the artists to only have to work within their own module,
keeping the artwork as dynamic as possible. An example would be battle stage
subteam that probably only had to work on the effects, scenes and visuals of the
Battle module.

.. note:: **Banking:**
    The module system only allows a single point of entry into, and a single
    point of exit out of, each distinct screen of the game.
    The PSX, which the game was originally developed for, had very limited
    resources. With only 1 Mib of Video RAM and 2 Mib of System RAM, data had
    to be banked in and out efficiently.
    Modules were a clean way to dump whole parts of the engine to make way for
    other parts.

Core Modules
============

The Core system is made up of six modules.

The are arranged in the following order :

* The Kernel, managing the whole system state.
* The Field Module, used by the exploration phase in 3D maps with 2D
  backgrounds.
* The Menu Module, to manage elements, like items, with 2D user interfaces.
* The Battle Module, displaying 3D battle scenes and action.
* The World Map Module, displaying the 3D overworld environment.
* The Mini Games Module, displaying specialized 3D environments for short
  in-game games.

Modules switching workflow
==========================

Not all modules are accessible by every other module.
There is a distinct flow between them and a module can only call
(or be called) by a subset of the other modules. For example :

* You can't access the item menu in the Menu Module from the battle module.
* Neither can you start a battle from the Mini Game module.

The Field Module is an exception because of its central role in the game.
It has a powerful scripting system that can call any module of the game. 
It may require the player to name a character, switch to world map on
current field exit, launch a random encounter battle or even starting a
mini game.


.. rubric:: Sources

* `Qhimm's Wiki <http://wiki.ffrtt.ru/index.php?title=FF7/Engine_basics>`_
