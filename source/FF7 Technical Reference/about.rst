About this project
******************

This project is intended to be a technical reference of how the
Final Fantasy VII game's internal gears works.
It is meant to be paired with the gameplay reference to provide a complete
overview of the game to all tech-oriented or curious players.

What about Qhimm's wiki?
========================

.. warning::
   Do **NOT** consider this content more up to date than Qhimm's Wiki, forums,
   the Q-Gears PDF or information that could be found in people's source code. 

* Once again, I played Final Fantasy VII.
* Once again, I felt the need to understand how it works in the inside. You know
  why: for science!
* Once again, I remembered works of the folks at
  `Qhimm's forums <https://forums.qhimm.com/>`_ or others.
* Once again, I hopped on the `Qhimm's Wiki <http://wiki.ffrtt.ru/>`_ [#f1]_ to
  get some information.
* Once again, I was... disappointed.

Not about the content nor it's quality ! Don't get me wrong.
It is a matter of taste I suppose, but I don't like reading this technical
documentation in a Wiki.
This is just an attempt to clear things up. For me at least, and maybe for
others.

I do not want to shadow anyone's work but rather build on the giant's shoulders.
I will *try* to reference my sources as much as I can.

If you feel that your work is not correctly or insufficiently referenced, please
open a pull request on this project.
You can add links to your work or sources: it would be great for others to build
upon your informations!
Obviously, I'll merge your PR as soon as I can.

Thanks again to all of you for sharing your valuable knowledge with everyone.
And thanks to Squaresoft / Square-Enix for having made such a great game.

.. toctree::
   :hidden:

   why


Hey, that is a nice idea! I want to contribute!
===============================================

**Please do!** Open a pull request on this repository and, once reviewed with my
*limited* knowledge, it will be added!

.. [#f1] This is a backup wiki. The original `wiki <https://wiki.qhimm.com/>`_
         went down at the end of 2017 and some content was lost.
         (`Source <http://forums.qhimm.com/index.php?topic=18025.msg254885#msg254885>`_)
