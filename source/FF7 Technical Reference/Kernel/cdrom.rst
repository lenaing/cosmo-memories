CD-ROM management
=================

PSX CD-ROM management
---------------------

One of the big rules on PSX development is that direct hardware access is
prohibited. Everything must go through the BIOS or the program will risk being
incompatible with later systems. This means not only to a new generation console
like PSX to PS2, but also all the trivial hardware revisions of the current
generation as well.

This create a problem for the kernel.

During module transitions (for example, going from Field module to Battle
module), the engine actually "preloads" the next module while the current one
is still executing.

This loading of data can not be done with a simple ``open()`` or ``read()``
BIOS syscall: whenever you enter the BIOS, the rest of the system comes to
a screeching halt until it is exited.

The problem is solved by FF7 actually controlling the CD-ROM access itself
through faster, low-level BIOS calls. In this "quick mode", the kernel can only
load 8 Kib of data at a time and also only references files by what *sector* of
the CD-ROM the data is located on, not by its *filename*.