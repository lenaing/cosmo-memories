VRAM management
===============

The kernel is in charge of allocating, caching and displaying pixels as data
stored in the Video RAM.

PSX VRAM management
-------------------

In the case of the PSX, the Playstation only has a 1 Mib of VRAM which make the
task a little complex. This is alleviated somewhat by using the PSX's VRAM
caching system.

The PSX video memory can best be seen as a rectangular "surface" made up of
2048x512 pixels.
A slight caveat to this mode is that the PSX can hold multiple color depths in
VRAM at the same time.

.. note::
  This document represents VRAM as a 1024x512 matrix to allow for some color depth
  in either direction and to minimize some extreme skewing of the video buffers.

The following is a typical state of VRAM during gameplay.

.. image:: /_static/tech/kernel/vram.png
   :align: center

The two game screens on the left side are the video buffer and the back buffer.
The patchwork of graphics on the top right are the field graphics for that
scene.
The bottom row consists of cached graphics, special effects, and semi-permanent
and permanent textures for the game.

The following is the VRAM with an additionnal descriptive frame.

.. image:: /_static/tech/kernel/vram2.png
   :align: center

Here the sections of the VRAM are easier to visualize:

* The large cyan areas are the video frame buffers. The PSX uses a standard
  double page buffer to animate the game.
* The empty areas above and below the frame buffers are blank to allow for a
  correct V-sync.
* The dark blue areas to the right of the frame buffers are used when the game
  plays 24-bit movies. This require a slightly larger display and the first
  two texture caches are overriden. During time in the game where no movies can
  take place, such as battles, textures are commonly placed here.
* The magenta area under the frame buffers is the Color Lookup Table (CLUT).
  This is where the texture palettes are stored. This also allow the PSX to
  display multiple color depths at the same time.
* The red area to the right is extra CLUT space when it is needed and when there
  are no texture currently cached there.
* The green area on the right is the permanent menu textures.
* The yellow area is where the menu font is stored.
* All the blank rectangles are the texture cache boundaries. In order of
  volatility:
  * The top two rows of cache space are overwritten from left to right
  * Then the bottom rows are overwritten
  * The texture on the bottom right are therefore barely overwritten except for key places.