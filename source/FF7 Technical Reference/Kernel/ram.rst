RAM management
==============

No matter what module is present in memory, there is a section
reserved for all variables for the entire game.

This is the *Savemap*. A *memory map* of all elements that represent the state
of the current game. When it is time to save a game, this section of memory
is copied to non-volatile RAM, such as a memory card (PSX) or a hard disk (PC).

The Savemap is 4380 (0x10F4) bytes long.

It contains 5 banks of 256 bytes of memory that are directly accessible by the
Field Scripting Language. These banks can be either accessed by a byte (8 bits)
or 2 bytes (16 bits) at a time, depending on the FSL command argument.

It also contains 256 bytes to store temporary field variables. These are not
used by field files to share data and are not persisted when the game is saved.

.. list-table::
   :widths: 10 10 10 20
   :header-rows: 1

   * - Offset
     - 8 Bit Field Bank
     - 16 Bit Field Bank
     - Description
   * - 0x0000
     - N/A
     - N/A
     - Beginning of the Savemap
   * - 0x0BA4
     - 0x1
     - 0x2
     - Field Script Bank 1
   * - 0x0CA4
     - 0x3
     - 0x4
     - Field Script Bank 2
   * - 0x0DA4
     - 0xB
     - 0xC
     - Field Script Bank 3
   * - 0x0EA4
     - 0xD
     - 0xE
     - Field Script Bank 4
   * - 0x0FA4
     - 0xF
     - 0x7
     - Field Script Bank 5
   * - 0x10F4
     - N/A
     - N/A
     - End of Savemap
   * -
     -
     -
     -
   * - N/A
     - 0x5
     - 0x6
     - Temporary field variables (256 bytes)
