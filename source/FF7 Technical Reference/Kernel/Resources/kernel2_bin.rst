kernel2.bin (PC)
****************

.. list-table::
   :widths: 10 10 10 10 10
   :header-rows: 1

   * - Version
     - Path / MD5
     - .. image:: /_static/tech/icons/flags/jp.png
     - .. image:: /_static/tech/icons/flags/us.png
     - .. image:: /_static/tech/icons/flags/fr.png
   * - .. image:: /_static/tech/icons/computer.png
        :alt: PC

       1998
     - ``/DATA/KERNEL/kernel2.BIN``
     -
     -
     -
   * - .. image:: /_static/tech/icons/steam.png
        :alt: Steam

       Steam
     - ``/DATA/KERNEL/kernel2.BIN``
     -
     -
     - ``7710f78b5633d5cb58a6a00f1b4cbaf6``

On the PC version, there exists a secondary kernel archive called
``kernel2.bin``. This archive contains only sections 10 to 27 of ``KERNEL.BIN``.

The data was uncompressed from the original archive, concatenated and compressed
with LZS into a single archive.

.. warning::
  The maximum allotted storage space on the PC version for all LZS uncompressed
  data in memory for ``kernel2.bin`` is 27 Kib (27648 bytes).
  This means that the total size of the extracted data (pointers and text)
  must be less than this.

.. rubric:: Sources

* `Qhimm's Wiki <http://wiki.ffrtt.ru/index.php?title=FF7/Kernel/Kernel.bin>`_
