KERNEL.BIN
**********

.. list-table::
   :widths: 10 10 10 10 10
   :header-rows: 1

   * - Version
     - Path / MD5
     - .. image:: /_static/tech/icons/flags/jp.png
     - .. image:: /_static/tech/icons/flags/us.png
     - .. image:: /_static/tech/icons/flags/fr.png
   * - .. image:: /_static/tech/icons/psx.png
        :alt: PSX

       PSX
     - ``/INIT/KERNEL.BIN``
     -
     -
     - ``af5ebff59a0cfece2d6abebaf140afe4``
   * - .. image:: /_static/tech/icons/computer.png
        :alt: PC

       1998
     - ``/DATA/KERNEL/KERNEL.BIN``
     -
     -
     -
   * - .. image:: /_static/tech/icons/steam.png
        :alt: Steam

       Steam
     - ``/DATA/KERNEL/KERNEL.BIN``
     -
     -
     - ``af5ebff59a0cfece2d6abebaf140afe4``

The file ``KERNEL.BIN`` is in BIN-GZIP format.

It consists of 27 gzipped sections concatenated together.

This file is the same for both the PSX and PC versions.

It holds all the static data and menu texts for the game.


.. list-table::
   :widths: 10 10 10 20 10
   :header-rows: 1

   * - Offset
     - Section
     - Description
     - File Format
     - Knowledge cover
   * - 0x0006
     - 1
     - :doc:`KERNEL.BIN/command_data`
     - BIN archive
     -
   * - 0x0086
     - 2
     - :doc:`KERNEL.BIN/attack_data`
     - BIN archive
     -
   * - 0x063A
     - 3
     - :doc:`KERNEL.BIN/battle_and_stats_data`
     - BIN archive
     -
   * - 0x0F7F
     - 4
     - :doc:`KERNEL.BIN/initialization_data`
     - BIN archive
     -
   * - 0x111B
     - 5
     - :doc:`KERNEL.BIN/item_data`
     - BIN archive
     -
   * - 0x137A
     - 6
     - :doc:`KERNEL.BIN/weapon_data`
     - BIN archive
     -
   * - 0x1A30
     - 7
     - :doc:`KERNEL.BIN/armor_data`
     - BIN archive
     -
   * - 0x1B73
     - 8
     - :doc:`KERNEL.BIN/accessory_data`
     - BIN archive
     -
   * - 0x1C11
     - 9
     - :doc:`KERNEL.BIN/materia_data`
     - BIN archive
     -
   * - 0x1F32
     - 10
     - Command descriptions
     -
     -
   * - 0x2199
     - 11
     - Magic descriptions
     -
     -
   * - 0x28D4
     - 12
     - Item descriptions
     -
     -
   * - 0x2EE2
     - 13
     - Weapon descriptions
     -
     -
   * - 0x307B
     - 14
     - Armor descriptions
     -
     -
   * - 0x315F
     - 15
     - Accessory descriptions
     -
     -
   * - 0x3384
     - 16
     - Materia descriptions
     -
     -
   * - 0x3838
     - 17
     - Key item descriptions
     -
     -
   * - 0x3BE2
     - 18
     - Command names
     -
     -
   * - 0x3CCA
     - 19
     - Magic names
     -
     -
   * - 0x4293
     - 20
     - Item names
     -
     -
   * - 0x4651
     - 21
     - Weapon names
     -
     -
   * - 0x4B02
     - 22
     - Armor names
     -
     -
   * - 0x4C4B
     - 23
     - Accessory names
     -
     -
   * - 0x4D90
     - 24
     - Materia names
     -
     -
   * - 0x5040
     - 25
     - Key item names
     -
     -
   * - 0x5217
     - 26
     - Battle and Battle Screen texts
     -
     -
   * - 0x5692
     - 27
     - Summon attack names
     -
     -

.. toctree::
    KERNEL.BIN/command_data
    KERNEL.BIN/attack_data
    KERNEL.BIN/battle_and_stats_data
    KERNEL.BIN/initialization_data
    KERNEL.BIN/item_data
    KERNEL.BIN/weapon_data
    KERNEL.BIN/armor_data
    KERNEL.BIN/accessory_data
    KERNEL.BIN/materia_data

.. rubric:: Sources

* `Qhimm's Wiki <http://wiki.ffrtt.ru/index.php?title=FF7/Kernel/Kernel.bin>`_
