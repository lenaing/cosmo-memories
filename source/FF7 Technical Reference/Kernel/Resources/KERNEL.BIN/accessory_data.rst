Accessory Data
**************

This section contains the Accessory data.

Each record is 16 bytes long.

.. list-table::
   :widths: 10 10 20
   :header-rows: 1

   * - Offset
     - Length
     - Description
   * - 0x00
     - 2 bytes
     - :ref:`bonus-stats`
   * - 0x02
     - 2 bytes
     - Bonus Amount
   * - 0x04
     - 1 byte
     - :ref:`elemental-strength`
   * - 0x05
     - 1 byte
     - :ref:`special-effect`
   * - 0x06
     - 2 bytes
     - :ref:`Elements protection bitmask <elements>`
   * - 0x08
     - 4 bytes
     - :ref:`Status protection bitmask <status-effects>`
   * - 0x0C
     - 2 bytes
     - :ref:`Equip bitmask <equip-mask>`
   * - 0x0E
     - 2 bytes
     - :ref:`Usage restriction bitmask <restriction-mask>`

.. _bonus-stats:

Bonus Stats
==================

.. list-table::
   :widths: 10 20
   :header-rows: 1

   * - Value
     - Stat
   * - 0xFF
     - None
   * - 0x00
     - Strength
   * - 0x01
     - Vitality
   * - 0x02
     - Magic
   * - 0x03
     - Spirit
   * - 0x04
     - Dexterity
   * - 0x05
     - Luck

.. _elemental-strength:

Elemental Strength
==================

.. list-table::
   :widths: 10 20
   :header-rows: 1

   * - Value
     - Protection Strength
   * - 0x00
     - Absorb
   * - 0x01
     - Nullify
   * - 0x02
     - Halve

.. _special-effect:

Special Effect
==============

.. list-table::
   :widths: 10 20
   :header-rows: 1

   * - Value
     - Special Effect
   * - 0xFF
     - None
   * - 0x00
     - *Haste*
   * - 0x01
     - *Berserk*
   * - 0x02
     - Curse Ring
   * - 0x03
     - *Reflect*
   * - 0x04
     - Increase Stealing Rate
   * - 0x05
     - Increase Manipulation Rate
   * - 0x06
     - *Barrier* / *MBarrier*

.. rubric:: Sources

* `Qhimm's Wiki <http://wiki.ffrtt.ru/index.php?title=FF7/Accessory_data>`_
