Item Data
*********

This section contains the item data.

Each record is 28 bytes long.

.. list-table::
   :widths: 10 10 20 20
   :header-rows: 1

   * - Offset
     - Length
     - Description
     - Notes
   * - 0x00
     - 8 bytes
     - Unknown
     - Always ``0xFFFFFFFFFFFFFFFF``
   * - 0x08
     - 2 bytes
     - Camera Movement ID
     - For single and multiple target attack
   * - 0x0A
     - 2 bytes
     - :ref:`Usage restriction bitmask <restriction-mask>`
     -
   * - 0x0C
     - 1 byte
     - :ref:`target-flags`
     -
   * - 0x0D
     - 1 byte
     - :doc:`Attack Visual Effect ID </FF7 Technical Reference/Codices/Attacks Codex/visual_effects>`
     -
   * - 0x0E
     - 1 byte
     - Damage Calculation
     -
   * - 0x0F
     - 1 byte
     - Item power for damage calculation
     -
   * - 0x10
     - 1 bytes
     - :ref:`sub-menu`
     -
   * - 0x11
     - 1 bytes
     - :ref:`status-effect-change`
     -
   * - 0x12
     - 1 byte
     - Attack Additional Effects
     -
   * - 0x13
     - 1 byte
     - Attack Additional Effects Modifier
     -
   * - 0x14
     - 4 bytes
     - :ref:`Attack status effect <status-effects>`
     -
   * - 0x18
     - 2 bytes
     - :ref:`Attack element <elements>`
     -
   * - 0x1A
     - 2 bytes
     - Special Attack Flags
     -

.. _sub-menu:

Condition sub menu
==================

.. list-table::
   :widths: 10 20
   :header-rows: 1

   * - Value
     - Description
   * - 0x00
     - Party HP
   * - 0x01
     - Party MP
   * - 0x02
     - Party Status
   * - Other
     - None

.. _status-effect-change:

Status Effect Change
====================

.. list-table::
   :widths: 10 20
   :header-rows: 1

   * - Value
     - Description
   * - 0x3F
     - Chance to inflict / cure status (out of 64)
   * - 0x40
     - Cure if was inflicted
   * - 0x80
     - Cure if was inflicted, inflict otherwise

.. rubric:: Sources

* `Qhimm's Wiki <http://wiki.ffrtt.ru/index.php?title=FF7/Item_data>`_
