Materia Data
************

This section contains the Materia data.

Each record is 20 bytes long.

.. list-table::
   :widths: 10 10 20
   :header-rows: 1

   * - Offset
     - Length
     - Description
   * - 0x00
     - 8 bytes
     - Level-UP AP limits, multiples of 100 (4 * 16-bit integer)
   * - 0x08
     - 1 byte
     - :ref:`equip-effects`
   * - 0x09
     - 3 bytes
     - :ref:`Status effect <status-effects>`, only first 24
   * - 0x0C
     - 1 byte
     - :ref:`Element index <elements>`
   * - 0x0D
     - 1 byte
     - Materia Type
   * - 0x0E
     - 1 byte
     - Materia Attributes
   * - 0x0F
     - 1 byte
     - Materia Attributes
   * - 0x10
     - 1 byte
     - Materia Attributes
   * - 0x11
     - 1 byte
     - Materia Attributes
   * - 0x12
     - 1 byte
     - Materia Attributes
   * - 0x13
     - 1 byte
     - Materia Attributes

.. _equip-effects:

Equip Effects
=============

.. list-table::
   :widths: 5 5 5 5 5 5 5 5 5
   :header-rows: 1

   * - Byte
     - STR
     - VIT
     - MAG
     - MDEF [#f1]_
     - DEX
     - LUCK
     - MAXHP
     - MAXMP
   * - 0x00
     -
     -
     -
     -
     -
     -
     -
     -
   * - 0x01
     - -02
     - -01
     - +02
     - +01
     -
     -
     - -05%
     - +05%
   * - 0x02
     - -04
     - -02
     - +04
     - +02
     -
     -
     - -10%
     - +10%
   * - 0x03
     -
     -
     -
     -
     - +02
     - -02
     -
     -
   * - 0x04
     - -01
     - -01
     - +01
     - +01
     -
     -
     -
     -
   * - 0x05
     - +01
     - +01
     - -01
     - -01
     -
     -
     -
     -
   * - 0x06
     -
     - +01
     -
     -
     -
     -
     -
     -
   * - 0x07
     -
     -
     -
     -
     -
     - +01
     -
     -
   * - 0x08
     -
     -
     -
     -
     -
     - -01
     -
     -
   * - 0x09
     -
     -
     -
     -
     - -02
     -
     -
     -
   * - 0x0A
     -
     -
     -
     -
     - +02
     -
     -
     -
   * - 0x0B
     - -01
     -
     - +01
     -
     -
     -
     - -02%
     - +02%
   * - 0x0C
     -
     -
     - +01
     -
     -
     -
     - -02%
     - +02%
   * - 0x0D
     -
     -
     - +01
     - +01
     -
     -
     - -05%
     - +05%
   * - 0x0E
     -
     -
     - +02
     - +02
     -
     -
     - -10%
     - +10%
   * - 0x0F
     -
     -
     - +04
     - +04
     -
     -
     - -10%
     - +15%
   * - 0x10
     -
     -
     - +08
     - +08
     -
     -
     - -10%
     - +20%
   * -
     -
     -
     -
     -
     -
     -
     -
     -
   * - 0x11 [#f2]_
     -
     -
     -
     -
     -
     -
     -
     -
   * - 0x12
     -
     -
     -
     -
     -
     -
     -
     -
   * - 0x13
     -
     -
     -
     -
     -
     -
     -
     -
   * - 0x14
     -
     -
     -
     -
     -
     -
     -
     -
   * - 0x15 [#f3]_
     - +01
     - +02
     - +04
     - +08
     - +16
     - +32
     - +64%
     - +128%

.. [#f1] Although the Materia equip menu will claim that certain materia
  increase the Magic Def stat, they really increase Spirit.
.. [#f2] Values from 0x11 to 0x15 are valid bonus in the PC version, but they are unused.
.. [#f3] Although allowable in the PC version, they is technically a memory leak.

Materia Types
=============

Materia is separated into 5 categories of functions in the game:

* Command (Yellow)
* Magic (Green)
* Summon (Red)
* Independent (Blue)
* Support (Purple)

The type is determined by a single byte value that is separated in two parts.

The upper nibble will be considered the sub-type and the lower nibble is the
base type.

Base Types
----------

.. list-table::
   :widths: 5 10
   :header-rows: 1

   * - Base
     - Materia Type
   * - 0x0
     - Independent
   * - 0x1
     - Independent
   * - 0x2
     - Command
   * - 0x3
     - Command
   * - 0x4
     - Independent
   * - 0x5
     - Support
   * - 0x6
     - Command
   * - 0x7
     - Command
   * - 0x8
     - Command
   * - 0x9
     - Magic
   * - 0xA
     - Magic
   * - 0xB
     - Summon
   * - 0xC
     - Summon
   * - 0xD
     - Independent [#f4]_
   * - 0xE
     - Independent
   * - 0xF
     - Independent

.. [#f4] There are no 0xD, 0xE or 0xF base types materias in the game. They are
  graphically represented as *Independent* but have no effect.

.. rubric:: Sources

* `Qhimm's Wiki <http://wiki.ffrtt.ru/index.php?title=FF7/Materia_data>`_
