Status Effects
==============

There are 31 status effects in FF7.

* In attacks,
  :doc:`items </FF7 Technical Reference/Kernel/Resources/KERNEL.BIN/item_data>`
  and
  :doc:`accessories </FF7 Technical Reference/Kernel/Resources/KERNEL.BIN/accessory_data>`
  they are referred to as a 32-bit bitmask.
* In
  :doc:`materia </FF7 Technical Reference/Kernel/Resources/KERNEL.BIN/materia_data>`
  they are stored as a 24-bit bitmask so the last 8 status can not be used.
* In weapon and armor they are represented as an index. Weapons have a low inflict chance.

.. list-table::
   :widths: 10 10 20
   :header-rows: 1

   * - Bit
     - Index
     - Status
   * - 0x00000001
     - 0x00
     - Death
   * - 0x00000002
     - 0x01
     - Near Death
   * - 0x00000004
     - 0x02
     - Sleep
   * - 0x00000008
     - 0x03
     - Poison
   * - 0x00000010
     - 0x04
     - Sadness
   * - 0x00000020
     - 0x05
     - Fury
   * - 0x00000040
     - 0x06
     - Confu
   * - 0x00000080
     - 0x07
     - Silence
   * - 0x00000100
     - 0x08
     - Haste
   * - 0x00000200
     - 0x09
     - Slow
   * - 0x00000400
     - 0x0A
     - Stop
   * - 0x00000800
     - 0x0B
     - Frog
   * - 0x00001000
     - 0x0C
     - Small
   * - 0x00002000
     - 0x0D
     - Slow Numb
   * - 0x00004000
     - 0x0E
     - Petrify
   * - 0x00008000
     - 0x0F
     - Regen
   * - 0x00010000
     - 0x10
     - Barrier
   * - 0x00020000
     - 0x11
     - M-Barrier
   * - 0x00040000
     - 0x12
     - Reflect
   * - 0x00080000
     - 0x13
     - Dual
   * - 0x00100000
     - 0x14
     - Shield
   * - 0x00200000
     - 0x15
     - D.Sentence
   * - 0x00400000
     - 0x16
     - Manipulate
   * - 0x00800000
     - 0x17
     - Berserk
   * -
     -
     -
   * - 0x01000000
     - 0x18
     - Peerless
   * - 0x02000000
     - 0x19
     - Paralysis
   * - 0x04000000
     - 0x1A
     - Darkness
   * - 0x08000000
     - 0x1B
     - Dual Drain
   * - 0x10000000
     - 0x1C
     - DeathForce
   * - 0x20000000
     - 0x1D
     - Resist
   * - 0x40000000
     - 0x1E
     - *Lucky Girl*
   * - 0x80000000
     - 0x1F
     - Imprisoned

.. note::
  Section 4 of Terence Fergusson's `Battle Mechanics FAQ <https://gamefaqs.gamespot.com/ps/197341-final-fantasy-vii/faqs/22395>`_
  describe each of these statuses with great detail.

.. warning::
  If *Dual Rain* is inflicted without *Dual*, the game will crash.