Targeting Data
**************

.. _target-flags:

Target Flags
============

Rows and viable targets
-----------------------

In any battle there are 2 or 3 rows.

* Most normal battles have 2 rows: allies and enemies.
* Pincer Attacks and Side Attacks have 3 rows: 2 enemy rows for the Pincer
  Attacks and 1 for Side Attacks.

Under most conditions, a "viable target" is a battle participant (enemy or ally)
that is not inflicted with the *Death* status.

Flags
-----

Targeting is defined by a single byte with eight different effects for each bit
that is set:

.. list-table::
   :widths: 10 10 20
   :header-rows: 1

   * - Bit
     - Effect on Target Selection
     - Description
   * - 0x01
     - Enable selection
     - Cursor will move to the battle field and a target can be selected with
       the other bits constraints.
   * - 0x02
     - Enable enemy selection
     - Cursor will start on the first enemy row.
   * - 0x04
     - Multiple targets
     - Cursors will select all targets in a given row.
   * - 0x08
     - Toggle Multiple / Single targets
     - Caster can switch cursor between multiple targets or single target.
       Damage (or cure) will be split among selected targets.
   * - 0x10
     - Single row
     - Cursor will only target allies or enemies as defined in 0x02 and can not
       be moved from the row.
   * - 0x20
     - "Short Range" (Physical damage only)
     - If the target of the caster is not in the front of their row, the target
       will take half damage.
       Also, selection is constrained by the :ref:`cover-flags`.
   * - 0x40
     - Battle scene
     - Cursor will select all viable targets of the battle scene.
   * - 0x80
     - Random target among selected
     - When multiple targets are selected, one will be selected at random to be
       the receiving target. Cursor will cycle among all viable targets.

Targets flags are set in 4 places :

* Commands
* Attacks
* Weapons
* :doc:`Items </FF7 Technical Reference/Kernel/Resources/KERNEL.BIN/item_data>`

The Command's target trumps any target information provided by the Attack or the
Weapon. As such, it is analyzed first. Any additional settings are added
through the Weapon or Attack.

This is further trumped by *Confuse* and *Berserk* which will set the target
data to 0xC0 and 0x97 respectively. *Confuse* will pick any available command to
perform and *Berserk* will just attack with the equipped weapon.

New target
----------

Although the cursor can move to an ally with the *Death* status, if no targets
selected to receive the attack are viable the attack will renew its target.

A dead enemy cannot be targeted, dead allies can. This is true for monsters and
party characters. A target renew resolution takes the original targeting flags
`AND` it with multiple targets choice, then random is added.

.. rubric:: Example 1

*Fire* (Target = 0x0F) is selected to be performed on an enemy. That action is
put into the ATB actions queue. But the enemy dies before *Fire* is performed.

*Fire*'s targeting data is then AND'd with 0x07 (*Multiple + Enemy + Selection*)
then 0x80 (*Random*) is added::

    New target = (0x0F & 0x07) + 0x80 = 0x87 # Random enemy

.. rubric:: Example 2

*Cure* (Target = 0x0D) is queued to heal a suffering ally. Before *Cure* is cast
the ally dies. *Cure* renew its target::

    New target = (0x0D & 0x07) + 0x80 = 0x85 # Random ally

.. note:: Enemy AI
  Setting a target in enemy attack does not control how the enemy selects
  targets. That is defined in their AI. Targets in enemy attack data only get
  used while they are being manipulated with the *Manipulate* command.

Sample Target Flags
-------------------

.. list-table::
   :widths: 10 10 20
   :header-rows: 1

   * - Flags
     - Set on
     - Description
   * - 0x00
     - *Defend* / *Change*
     - This will only allow the caster to perform the command on itself.
   * - 0x01
     - *Shield*
     - This will only allow the caster to perform the spell on itself or one
       ally.
   * - 0x17
     - Summons
     - A PC cannot perform a summon that attacks on his/her own character or
       row. It will only target all of an enemy row, but only one row.
   * - 0x15
     - Some of Aeris's and Cait Sith's Limit Breaks
     - Same as above, but only on the caster's row.
   * - 0x23
     - Most weapons
     - This make some of the weapons into a *short range* attack. If the *Long
       Range* Materia is equipped, bit 0x20 of its targeting data is forced to
       0.
   * - 0x97
     - *Comet2* / *Berserk*
     - This will target one target of the enemy row at random.
   * - 0xC7
     - *Roulette*
     - Will choose one target at random from among all battle participants not
       inflicted with the *Death* status.

.. _cover-flags:

Cover Flags
===========

These flags are used in conjunction with row to determine if a target can be
selected as the target of a *short range* attack.

The determination is worked out in this way:

* An enemy exists in row 1 and another in row 2.
* If any enemy in row 1 shares a cover flag with the enemy in row 2, then the enemy in row 2 cannot be targeted until all enemies in row 1 that share a cover flag with him are defeated::

    if ((B_row > A_row) && ((B_flags & A_flags) > 0) {
      # Enemy cannot be targeted by short ranged attacks.
    }

.. rubric:: Sources

* `Qhimm's Wiki : Targeting Data <http://wiki.ffrtt.ru/index.php?title=FF7/Battle/Targeting_Data>`_
* `Qhimm's Wiki : Cover Flags <http://wiki.ffrtt.ru/index.php?title=FF7/Battle/Battle_Scenes#Binary_.22Cover_Flags.22>`_

