Game Mechanics
**************

Characters
==========

.. _equip-mask:

Equip mask
----------

Weapons, armors and accessories can be equipped only on some characters.
This is usualy specified through a bitmask:

.. list-table::
   :widths: 10 20
   :header-rows: 1

   * - Bit
     - Equipable on
   * - 0x0001
     - Cloud
   * - 0x0002
     - Barret
   * - 0x0004
     - Tifa
   * - 0x0008
     - Aeris
   * - 0x0010
     - Red XIII
   * - 0x0020
     - Yuffie
   * - 0x0040
     - Cait Sith
   * - 0x0080
     - Vincent
   * - 0x0100
     - Cid
   * - 0x0200
     - Young Cloud
   * - 0x0400
     - Sephiroth

Battle
======

.. toctree::
  Battle/targeting_data

.. include::
  elements.rst

.. _status-effects:

.. include::
  status_effects.rst

Weapon, Armor, Accessories and Items
====================================

.. _restriction-mask:

Usage Restriction mask
----------------------

Some weapons, armors or accessories can only be used in battle. Some others only
outside of them. And some can be sold. All these states are handled by a
specific bitmask where effects applies if there specified bits are 0.

.. list-table::
   :widths: 10 20
   :header-rows: 1

   * - Mask part
     - Description
   * - 0x01
     - Can be sold
   * - 0x02
     - Can be used in battle
   * - 0x04
     - Can be used out of battle

.. rubric:: Sources

* `Qhimm's Wiki : Elements <http://wiki.ffrtt.ru/index.php?title=FF7/Battle/Elemental_Data>`_
* `Qhimm's Wiki : Status Effects <http://wiki.ffrtt.ru/index.php?title=FF7/Battle/Status_Effects>`_
