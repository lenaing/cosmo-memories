.. _elements:

Elements
========

There are 16 elements in FF7.

* They are usually stored as a bitmask.
* In materia they are stored through their index.

.. list-table::
   :widths: 10 10 20
   :header-rows: 1

   * - Bit
     - Index
     - Element
   * - 0x0001
     - 0x00
     - Fire
   * - 0x0002
     - 0x01
     - Ice
   * - 0x0004
     - 0x02
     - Bolt
   * - 0x0008
     - 0x03
     - Earth
   * - 0x0010
     - 0x04
     - Poison
   * - 0x0020
     - 0x05
     - Gravity
   * - 0x0040
     - 0x06
     - Water
   * - 0x0080
     - 0x07
     - Wind
   * - 0x0100
     - 0x08
     - Holy
   * -
     -
     -
   * - 0x0200
     - 0x09
     - Restorative
   * - 0x0400
     - 0x0A
     - Cut
   * - 0x0800
     - 0x0B
     - Hit
   * - 0x1000
     - 0x0C
     - Punch
   * - 0x2000
     - 0x0D
     - Shoot
   * - 0x4000
     - 0x0E
     - Shout
   * -
     -
     -
   * - 0x8000
     - 0x0F
     - Hidden / Ultima


Physical and Non-Physical elements
----------------------------------

Elements 0x0A to 0x0E are considered *Physical* elements, though not enforced
as such and show up on physical attacks.

Non-elemental attacks, such as Bahamut's Flares, have no element selected.

.. note::
  **Display**: The names of elements 0x09 to 0x0E are never mentionned in the
  game. However, they are stored as battle text for the purpose of sensing.
  *Hidden / Ultima* has no name mentioned in the battle text.
