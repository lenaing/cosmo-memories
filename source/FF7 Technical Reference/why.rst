Why ?
*****

Here is a little story
======================

I remember watching one of my brothers play Final Fantasy VII in 1997 on our CRT
TV. I was only 10 at that time. However, I still have a lot of memories of these
moments : a great scenario, fantastic universe, awesome music, georgous 3D
graphics and a game system that looked like a bit of a classic pen and paper
RPG.

I was thrilled. Goosebumped. However I was also too young to fully understand
all the game mechanics and be able to play by myself. Then my brother moved out
and left with his PSX.
So I wished that I could have my own video game console someday.

Fast forward: September 2001. Once we got our first personnal computer at home,
I was loaned a digital copy of FF7 by a friend.
That must have been the first computer video game that I installed on a modern
computer. 5 years after the first impact, I got this feeling again, played hours
and days, finished it. Felt in love with it. And sadly had to return it to my
friend.

When FF8 hit the PC stores in 2000, I made enough pocket money to buy it by
myself.
I liked it, but I wasn't has hooked as the first time I played FF7.
Non chibi characters, a dull magic system, a kind of "what the f..?" story.
Well. I was disappointed to say the least.

Then I realised that if there was an 8 and a 7, there must have been something
from 1 to 6?
Sadly, episodes 1 to 6 never reached Europe. Actually, some of them never
reached North America or were relabeled! But, I had the chance to have a Dia-lup
Internet access.
Discovered emulators like `NESticle <https://en.wikipedia.org/wiki/NESticle>`_
or `Snes9X <https://www.snes9x.com/>`_. Found ROMs. Guess how I learned English?
At that time, there was merely no fan translation in French!

Playing theses games confirmed me that I liked these turn-based JRPG systems.
But then I had to go to high school / boarding school and laptops weren't
affordable, so I learned harshly the frustration of not having the chance to
play when I want.
Fortunately, I got a Gameboy, but no Final Fantasy on this one either for
Europe!

2005: FF7 Advent Children hit the theatres. That was *beautiful*.
Also 2005, FF7 was a technical demo for PS3. THAT WAS *MAGNIFICENT*.

In 2007, I went to Germany with a CS friend to work on our final CS project.
We had laptops. He had FF7 installed on it. We had a lot of free time and, now,
we knew how to code.
I hate not knowing how things work and playing it again made me want to know
more.

Then I felt myself falling in a well of science.
`Qhimm's website <https://qhimm.com/>`_.
Source codes. Tweaks. Structures. So much things to try and discover.
I remember working on a ``KERNEL.BIN`` unpacker, but my code knowledge still was
lacking a lot of concepts and patterns.

Internship ended, life moved on.

Sometimes, I felt the need to play it again. And again. And again.
Now grown up, I had the chance to buy my own PSX, playing on the "real thing"
again. Tried FF9, but was not so attracted to it.
Wishing that "one day", an FF7 remake would be something real.

Even now I'm still not tired of playing it from time to time.
I got back a few times to Qhimm's forums, delighted by the work of people
tinkering with the data, making mods, sharing knowledge.
Saw the `Q-Gears <http://q-gears.sourceforge.net/>`_ project rise and... well
not fall, but stall :)
I lurked around, discovered the wiki, saw it disappear and reappear...

Then, in 2015, FF7 Remake is officially announced. After 5 years of waiting and
23 years after the PSX version, I got my hands on the first episode of FF7
Remake on PS4. I received my First Class package with the awesome figurine of
Cloud in the middle of the first french Covid-19 lockdown.

I guess that, yes, I'm pretty fond of the Final Fantasy games series.
I believe that the 7th episode and its remake may be some of the best
technically, scenaristically and eyecandy video games of their time.

And now...
==========

Here we are. France, November 7th 2020. Second lockdown in France.
"Plenty" of time ahead of me to spend on... well, something that could be a bit
more fun than the awful health situation.

Tidying up my room, I see my old PSX Game Library, putting my FF7 Remake disc
next to it after reaching the platinum.

Once again, my desire to play and tinker with FF7 manifests itself.
Let's try something new...

> Lenain