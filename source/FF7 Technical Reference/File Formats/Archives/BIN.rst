BIN File Format
***************

The BIN format comes as two different types.
They both have the same ``.BIN`` extension, so one must open the file to see
which format is used. They are best described as BIN Types and BIN-GZIP types.

BIN Archive
===========

These are uncompressed archives.

The file has an 4 bytes long header that gives the length of the data, followed
by the data.

.. list-table::
   :widths: 10 10 10 20
   :header-rows: 1
   :stub-columns: 1

   * - Type
     - Offset
     - Length
     - Description
   * - Header
     - 0x0000
     - 4 bytes
     - Length of the data
   * - Data
     - 0x0004
     - Variable
     - Data

BIN-GZIP Archive
================

These archives store multiple gzipped files concatenated together.

Each file in the archive have a 6 bytes header followed by its gzipped data.

.. list-table::
   :widths: 10 10 10 20
   :header-rows: 1
   :stub-columns: 1

   * - Type
     - Offset
     - Length
     - Description
   * - Header
     - 0x0000
     - 2 bytes
     - Length of gzipped file n°1
   * -
     - 0x0002
     - 2 bytes
     - Length of uncompressed file n°1
   * -
     - 0x0004
     - 2 bytes
     - Type of file n°1
   * - Data
     - Variable
     - 2 bytes
     - Gzip Header and data of file n°1
   * - Header
     - Variable
     - 2 bytes
     - Length of gzipped file n°2
   * -
     - Variable
     - 2 bytes
     - Length of uncompressed file n°2
   * -
     - Variable
     - 2 bytes
     - Type of file n°2
   * - Data
     - Variable
     - Variable
     - Gzip Header and data of file n°2
   * - ...
     - ...
     - ...
     - ...

File Type
------------

The file type values seem to be unique to the particular archive opened and
are not consistent between archives.

It denote specific type of compressed files within an archive.

.. rubric:: Example 1

Within the ``KERNEL.BIN`` file, the first 9 files represent different data
sets, so they are numbered sequentially from 0 to 8. All remaining files
represent text data and are labeled as type 9.

.. rubric:: Example 2

Within the ``WINDOW.BIN`` file there are 3 files, the first 2 are textures and
numbered type 0. The third file is *not* a texture so it is marked as type 1.

.. rubric:: Sources

* `Qhimm's Wiki <http://wiki.ffrtt.ru/index.php?title=FF7/Kernel/Low_level_libraries#BIN_archive_data_format>`_
