Archives
********

To save space, quicken access time, and to obfuscate the file structure a
little, most of the FF7 data files are stored in some kind of archive format.

The archives remove such useful items as subdirectories and logical data
placement. There is no real "native" format these are based on.

.. toctree::

   BIN
   LGP
