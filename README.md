# Cosmo Memories

<img src="source/_static/FFVII.svg" align="center" width="300" />

A collection of reference sources about Final Fantasy VII.

Generated documentation is available [here](https://lenaing.gitlab.io/cosmo-memories/).

[![pipeline status](https://gitlab.com/lenaing/cosmo-memories/badges/master/pipeline.svg)](https://gitlab.com/lenaing/cosmo-memories/pipelines)

## How to build the documentation

Create a Python Virtual Env, and install requirements: 

```bash
$ python -m venv .venv
$ source .venv/bin/activate
$ pip install -r requirements.txt
```

Build the documentation, output will be in `build/html/`:

```bash
$ sphinx-build source/ build/
```

If you want live reloading while you modify the project:

```bash
$ sphinx-reload . --build-dir build/
```